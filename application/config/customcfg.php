<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 $config['main_dist']='/main_css_js/';
 $config['include']='/include/';
 $config['employee']='/employee/';
 $config['user']='/user/';
 $config['time_record']='/time_record/';
 $config['local_date']=date("Y-m-d");
 $config['local_time']=date("H:i:s");
 $config['local_datetime']=date("Y-m-d H:i:s");
 
 $config['row_per_page']=10;
 $config['number_display']=5;
 $config['file_version']="?1.0.0.0";
 $config['systemname']="DTR.COM";
