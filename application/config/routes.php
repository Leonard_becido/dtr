<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//Time Record
$route['dashboard/time_record'] = 'TimeRecordCtrl';

$route['dashboard/randompass'] = 'UserCtrl/random_password';
// Users
$route['dashboard/get_user_details/(:any)'] = 'UserCtrl/userDetails/$1';
$route['dashboard/delete_selected_user'] = 'UserCtrl/deleteSelectedUser';
$route['dashboard/delete_user/(:any)'] = 'UserCtrl/deleteUser/$1';
$route['dashboard/edit_user'] = 'UserCtrl/editUser';
$route['dashboard/save_user'] = 'UserCtrl/saveNewUser';
$route['dashboard/user'] = 'UserCtrl';
$route['dashboard/pass/(:any)'] = 'UserCtrl/password_check/$1';

// Employee
$route['dashboard/get_employee_details/(:any)'] = 'EmployeeCtrl/employeeDetails/$1';
$route['dashboard/delete_selected_employee'] = 'EmployeeCtrl/deleteSelectedEmployee';
$route['dashboard/delete_employee/(:any)'] = 'EmployeeCtrl/deleteEmployee/$1';
$route['dashboard/edit_employee'] = 'EmployeeCtrl/editEmployee';
$route['dashboard/save_employee'] = 'EmployeeCtrl/saveNewEmployee';
$route['dashboard'] = 'EmployeeCtrl';

// Time In Out
$route['time_in_out'] = 'DTRCtrl/timInOut';
$route['login/(:any)'] = 'DTRCtrl/signin/$1';
$route['logout'] = 'DTRCtrl/signout';

$route['default_controller'] = 'DTRCtrl';
$route['404_override'] = 'pagenotfoundctrl';
$route['translate_uri_dashes'] = FALSE;
