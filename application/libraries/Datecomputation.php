<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Datecomputation{
	
	public function diffhour($date1,$date2,$return){
         
        $d1 = strtotime($date1);
        $d2 = strtotime($date2);
        $totalSecondsDiff = abs($d1-$d2); 
        $totalMinutesDiff = $totalSecondsDiff/60; 
        $totalHoursDiff   = $totalSecondsDiff/60/60;
        $totalDaysDiff    = $totalSecondsDiff/60/60/24; 
        $totalMonthsDiff  = $totalSecondsDiff/60/60/24/30; 
        $totalYearsDiff   = $totalSecondsDiff/60/60/24/365; 
		switch ($return) {
		case "year":
			$val=$totalYearsDiff;
			break;
		case "month":
			$val=$totalMonthsDiff;
			break;
		case "day":
			$val=$totalDaysDiff;
			break;
		case "hour":
			$val=$totalHoursDiff;
			break;
		case "minute":
			$val=$totalMinutesDiff;
			break;
		case "second":
			$val=$totalSecondsDiff;
			break;
		}
         return $val;
     }
	
}