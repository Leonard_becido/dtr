<!--start add New Employee -->
<div id="addNewEmployeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addNewEmployeeLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="addNewEmployeeLabel">Add New Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $attributes = array('class' => 'custom-validation', 'id' => 'addNewEmploye','name' => 'addNewEmp','methor'=>'post');
                echo form_open('#',$attributes); ?>
                    <div class="form-group row mb-4">
                        <label for="txtfname" class="col-sm-3 col-form-label">First name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="txtfname" name="txtfname" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="txtlname" class="col-sm-3 col-form-label">Last name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="txtlname" name="txtlname" required autocomplete="off">
                        </div>
                    </div>
                   
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <?php
                $submit_url = base_url().'dashboard/save_employee';
                ?>
                <button type="button" id="save_btn" class="btn btn-primary waves-effect waves-light"  onclick="actionBtn('addNewEmploye','<?php echo  $submit_url ?>');">Save</button>
                

                <i style="display:none" id="loader_spinner">
                                                    <div class="spinner-border text-danger m-1" role="status" >
                                                        <span class="sr-only">Loading Please Wait...</span>
                                                    </div>Loading Please Wait...
                                                    </i>

                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>
<!--end add New Employee -->


<!--start edit Employee -->
<div id="editEmployeeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Edit Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <?php $attributes = array('class' => 'custom-validation', 'id' => 'editEmployee','name' => 'editEmp','methor'=>'post');
                echo form_open('#',$attributes); ?>
                 <input type="hidden" class="form-control" id="txteditid" name="txteditid" readonly>
                    <div class="form-group row mb-4">
                        <label for="txtfname" class="col-sm-3 col-form-label">First name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="txteditfname" name="txteditfname" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="txtlname" class="col-sm-3 col-form-label">Last name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="txteditlname" name="txteditlname" required autocomplete="off">
                        </div>
                    </div>
                   
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <?php
                $submit_url = base_url().'dashboard/edit_employee';
                ?>
                <button type="button" id="edit_btn" class="btn btn-primary waves-effect waves-light"  onclick="actionBtn('editEmployee','<?php echo  $submit_url ?>');">Save</button>
                

                <i style="display:none" id="loader_spinner">
                                                    <div class="spinner-border text-danger m-1" role="status" >
                                                        <span class="sr-only">Loading Please Wait...</span>
                                                    </div>Loading Please Wait...
                                                    </i>

                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>
<!--end edit Employee -->


<!--start view Employee -->
<div id="viewEmployeeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">View Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <img src="" id="empQR"width='30%'/>
            <?php $attributes = array('class' => 'custom-validation', 'id' => 'editEmployee','name' => 'editEmp','methor'=>'post');
                echo form_open('#',$attributes); ?>
                 <input type="hidden" class="form-control" id="txteditid" name="txteditid" readonly>
                    <div class="form-group row mb-4">
                        <label for="txtfname" class="col-sm-3 col-form-label">First name:</label>
                        <div class="col-sm-9">
                            <label class="col-form-label" id="txtviewfname" name="txtviewfname" ></label>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="txtlname" class="col-sm-3 col-form-label">Last name:</label>
                        <div class="col-sm-9">
                        <label class="col-form-label" id="txtviewlname" name="txtviewlname" ></label>
                        </div>
                    </div>
                   
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
               
                

            
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>
<!--end view Employee -->



