<!DOCTYPE html>
<html lang="en">

    <head>
        
        <meta charset="utf-8" />
        <title><?php echo $page_title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?php echo $main_css_js_dir ?>dist/assets/images/eswriting.ico">

        <?php if(isset($css_include)){
                foreach ($css_include as $page){?>
                    <link href="<?php echo $main_css_js_dir ?>dist/assets/<?php echo $page ?>" rel="stylesheet" type="text/css"/>
        <?php }
        }
        ?>
        
        <!-- Bootstrap Css -->
        <link href="<?php echo $main_css_js_dir ?>dist/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="<?php echo $main_css_js_dir ?>dist/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="<?php echo $main_css_js_dir ?>dist/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    </head>

    <body data-sidebar="dark" >

        <!-- Begin page -->
        <div id="layout-wrapper">

            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="d-flex">
                        <!-- LOGO -->
                        <div class="navbar-brand-box">
                            <a href="index.html" class="logo logo-dark">
                                <span class="logo-sm">
                                    <img src="<?php echo $main_css_js_dir ?>dist/assets/images/eswriting-logo.png" alt="" height="60">
                                </span>
                                <span class="logo-lg">
                                    <img src="<?php echo $main_css_js_dir ?>dist/assets/images/leswriting-logo.png" alt="" height="60">
                                </span>
                            </a>

                            <a href="index.html" class="logo logo-light">
                                <span class="logo-sm">
                                    <img src="<?php echo $main_css_js_dir ?>dist/assets/images/eswriting-logo.png" alt="" height="60">
                                </span>
                                <span class="logo-lg">
                                    <img src="<?php echo $main_css_js_dir ?>dist/assets/images/eswriting-logo.png" alt="" height="60">
                                </span>
                            </a>
                        </div>

                        <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                            <i class="fa fa-fw fa-bars"></i>
                        </button>

                        <!-- App Search-->
                        <!-- <form class="app-search d-none d-lg-block">
                            <div class="position-relative">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="bx bx-search-alt"></span>
                            </div>
                        </form> -->

                        

                    </div>

                    <div class="d-flex">

                        <!-- <div class="dropdown d-inline-block d-lg-none ml-2">
                            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="mdi mdi-magnify"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                                aria-labelledby="page-header-search-dropdown">
                    
                                <form class="p-3">
                                    <div class="form-group m-0">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> -->



                        

                        <!-- <div class="dropdown d-none d-lg-inline-block ml-1">
                            <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                                <i class="bx bx-fullscreen"></i>
                            </button>
                        </div> -->

                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" padding-right:20px;">
                                <i class="bx bx-bell bx-tada"></i>
                                <span class="badge badge-danger badge-pill">0</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                                aria-labelledby="page-header-notifications-dropdown">
                                <div class="p-3">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h6 class="m-0"> Notifications </h6>
                                        </div>
                                        <div class="col-auto">
                                            <!-- <a href="#!" class="small"> View All</a> -->
                                        </div>
                                    </div>
                                </div>
                                <div data-simplebar style="max-height: 230px;">
                                    

                                    <!-- <a href="" class="text-reset notification-item">
                                        <div class="media">
                                            <img src="<?php echo $main_css_js_dir ?>dist/assets/images/users/avatar-4.jpg"
                                                class="mr-3 rounded-circle avatar-xs" alt="user-pic">
                                            <div class="media-body">
                                                <h6 class="mt-0 mb-1">Salena Layfield</h6>
                                                <div class="font-size-12 text-muted">
                                                    <p class="mb-1">As a skeptical Cambridge friend of mine occidental.</p>
                                                    <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 1 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a> -->
                                </div>
                                <div class="p-2 border-top">
                                    <a class="btn btn-sm btn-link font-size-14 btn-block text-center" href="javascript:void(0)">
                                        <!-- <i class="mdi mdi-arrow-right-circle mr-1"></i> View More.. -->
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle header-profile-user" src="<?php echo $main_css_js_dir ?>dist/assets/images/users/user.jfif"
                                    alt="Header Avatar">
                                <!-- <span class="d-none d-xl-inline-block ml-1"><?php echo $user_detail['firstname'] ?></span> -->
                                <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                             
                                <a class="dropdown-item text-danger" href="<?php echo base_url().'logout'; ?>"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> Logout</a>
                            </div>
                        </div>

                        <!-- <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                                <i class="bx bx-cog bx-spin"></i>
                            </button>
                        </div> -->
            
                    </div>
                </div>
            </header>

            <!-- ========== Left Sidebar Start ========== -->
            <div class="vertical-menu">

                <div data-simplebar class="h-100">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu list-unstyled" id="side-menu">
                            <li class="menu-title">Menu</li>
                            <?php if($web_user_type==1){ ?>
                            <li>
                                <a href="<?php echo base_url().'dashboard'; ?>" class="waves-effect ">
                                    <i class="mdi mdi-account-group "></i>
                                    <span>Employee</span>
                                </a>
                               
                            </li>
                            <?php } ?>
                            <li>
                                <a href="<?php echo base_url().'dashboard/time_record'; ?>" class="waves-effect ">
                                    <i class="mdi mdi-clock "></i>
                                    <span>Time Record</span>
                                </a>
                               
                            </li>

                            
                            <?php if($web_user_type==1){ ?>
                            <li>
                                <a href="<?php echo base_url().'dashboard/user'; ?>" class="waves-effect">
                                    <i class="mdi mdi-account-key"></i><span class="badge badge-pill badge-info float-right"></span>
                                    <span>Users</span>
                                </a>
                               
                            </li>
                            <?php } ?>
                           
                            <li class="menu-title">My Account</li>
                           
                            <li>
                                <a href="<?php echo base_url().'logout'; ?>" class="waves-effect">
                                    <i class="bx bx-home-circle"></i>
                                    <span>Log-out</span>
                                </a>
                               
                            </li>

                           
                        </ul>
                    </div>
                    <!-- Sidebar -->
                </div>
            </div>
            <!-- Left Sidebar End -->
<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">
                       
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0 font-size-18"><?php echo $body_title ?></h4>

                                    <!-- <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Tables</a></li>
                                            <li class="breadcrumb-item active">Responsive Tables</li>
                                        </ol>
                                    </div> -->

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->