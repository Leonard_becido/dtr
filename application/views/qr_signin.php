<!DOCTYPE html>
<html lang="en">

    <head>
        
        <meta charset="utf-8" />
      
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $page_title ?></title>
        <meta content="" name="" />
     
        <!--<meta content="Themesbrand" name="author" />
         App favicon -->
        <link rel="shortcut icon" href="<?php echo $main_css_js_dir ?>dist/assets/images/eswriting.ico">
        <link href="<?php echo $main_css_js_dir ?>dist/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
        <!-- Bootstrap Css -->
        <link href="<?php echo $main_css_js_dir ?>dist/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="<?php echo $main_css_js_dir ?>dist/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="<?php echo $main_css_js_dir ?>dist/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
        
        <!-- <script type="text/javascript" src="<?php echo $main_css_js_dir ?>dist/assets/js/custom/instascan.min.js"></script> -->

        <script type="text/javascript" src="<?php echo $main_css_js_dir ?>dist/assets/js/custom/adapter.min.js"></script>
        <script type="text/javascript" src="<?php echo $main_css_js_dir ?>dist/assets/js/custom/vue.min.js"></script>
        <script type="text/javascript" src="<?php echo $main_css_js_dir ?>dist/assets/js/custom/instascan.min.js"></script>

    
       

        
    </head>

    <body>
    <div class="account-pages my-5 pt-sm-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card overflow-hidden">
                        <div class="bg-soft-primary">
                                <div class="row">
                                    <div class="col-8">
                                        <div class="text-primary p-4">
                                            <!-- <h5 class="text-primary"></h5>
                                            <p><br><i></i>.</p> -->
                                            <p><h2 class="text-primary"><strong id="dateclock"><?php echo $clock ?></strong></h2></p>
                                        </div>
                                    </div>
                                    <div class="col-4 align-self-end">
                                        <img src="<?php echo $main_css_js_dir ?>dist/assets/images/profile-img.png" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pt-1"> 
                                <div>
                                    <a href="index.html">
                                        <div class="avatar-md profile-user-wid mb-4">
                                            <span class="avatar-title rounded-circle bg-light">
                                                <img src="<?php echo $main_css_js_dir ?>dist/assets/images/clock.png" alt="" class="rounded-circle" height="100">
                                            </span>
                                        </div>
                                    </a>
                                </div>
                               

                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                                    <span class="d-none d-sm-block"><h5>DTR</h5></span>    
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#profile" role="tab">
                                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                                    <span class="d-none d-sm-block"><h5>Login</h5></span>    
                                                </a>
                                            </li>
                                           
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content p-3 text-muted">
                                            <div class="tab-pane active" id="home" role="tabpanel">
                                                <p class="mb-0">
                                                Scan Your QR Here
                                                <video id="preview" width="100%"></video>
                                             
                                                </p>
                                            </div>
                                            <div class="tab-pane" id="profile" role="tabpanel">
                                               
                                                <p class="mb-0">
                                                    <!-- Login User -->

                                                    <div class="p-2">

                                                    <?php if(isset($logerror)){?>
                                                    <div class="alert alert-danger" role="alert">
                                                        <i class="bx bxs-comment-error"></i> Invalid <a href="#" class="alert-link">Username or Password</a>.<br>Login Attempt <strong>2</strong>
                                                    </div>
                                                    <?php } ?>

                                                        <?php $attributes = array('class' => 'custom-validation', 'id' => 'loginfrm','methor'=>'post');
                                                        echo form_open('login/authentication', $attributes); ?>
                        
                                                        <div class="form-group">
                                                            <label for="username">Username</label>
                                                            
                                                            <input type="text" class="form-control" id="txtusername" name="txtusername" placeholder="Enter username" required autocomplete="off">
                                                        </div>
                                
                                                        <div class="form-group">
                                                            <label for="userpassword">Password</label>
                                                            <input type="password" class="form-control" id="txtuserpassword" name="txtuserpassword" placeholder="Enter password" required >
                                                        </div>
                                
                                                    
                                                        <div class="mt-3">
                                                            <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Log In</button>
                                                        </div>
                                                 <?php echo form_close() ?>
                                                </p>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
           
   

       
        <!-- JAVASCRIPT -->
        <script src="<?php echo $main_css_js_dir ?>dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="<?php echo $main_css_js_dir ?>dist/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo $main_css_js_dir ?>dist/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="<?php echo $main_css_js_dir ?>dist/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="<?php echo $main_css_js_dir ?>dist/assets/libs/node-waves/waves.min.js"></script>
        <!-- Sweet Alerts js -->
        <script src="<?php echo $main_css_js_dir ?>dist/assets/libs/sweetalert2/sweetalert2.min.js"></script>

        <!-- Sweet alert init js-->
        <script src="<?php echo $main_css_js_dir ?>dist/assets/js/pages/sweet-alerts.init.js"></script>
        <script type="text/javascript">
            let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
            
            Instascan.Camera.getCameras().then(function (cameras) {
                if (cameras.length > 0) {
                scanner.start(cameras[0]);
                } else {
                console.error('No cameras found.');
                }
            }).catch(function (e) {
                console.error(e);
            });

            scanner.addListener('scan', function (content) {
                // document.getElementById("txtQRCode").value = content;
                <?php $QRTimeInOutUrl = base_url().'time_in_out' ?>
                readQR('<?php echo $QRTimeInOutUrl ?>',content);
            });
      
       

       
            function readQR($url,QRCode){
               
                $.ajax({
                    url: $url,
                    type: 'POST',
                    xhr: function() {
                        var myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    success: function (data) {
                        if(data.STATUS =="success"){
                            Swal.fire({
                                position:"middle-center",
                                type:"success",
                                title:"Thank you "+data.EMPNAME+"<br>"+data.EMPTIME,
                                showConfirmButton:!1,timer:1500
                            });
                        }else{
                            Swal.fire({
                                position:"middle-center",
                                type:"warning",
                                title:"QR Code Not Found",
                                showConfirmButton:!1,timer:1500
                            });
                        }
                    },
                        error : function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("System Error Please reload the system ");
                    },
                    data: ({ QRCode:QRCode }),
                    cache: false,
                    dataType : 'json',
                    // contentType: false,
                    //  processData: false
                })
            }
            setInterval(function(){ 
                var current_page = window.location.href;
                $('#dateclock').load(current_page + ' #dateclock');
             }, 1000);

             
            
        </script>


       
        <!-- App js -->
        <!-- <script src="<?php echo $main_css_js_dir ?>dist/assets/js/app.js"></script> -->

       

    </body>
</html>
