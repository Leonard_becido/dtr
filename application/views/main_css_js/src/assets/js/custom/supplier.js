function check_input_new_supplier(){
	alert();
	var sup_company_name = $("#txtcompany_name").val();
	var supp_company_phone = $("#txtcompany_phone").val();

	if(sup_company_name=="" || supp_company_phone=="" ){
		$("#supp_details_err").show();
	}else{
		$("#supp_details_err").hide();
	}
}

//$("#sa-position").click(function(){

function save_now(action_link,frm,item_show,item_hide,error_list){

	var show_act = item_show.split(",");
	var hide_act = item_hide.split(","); 
	
	//hide
	var i;
	for (i = 0; i < hide_act.length; i++) {
	  $('#'+hide_act[i]).hide();
	}
	for (i = 0; i < show_act.length; i++) {
	  $('#'+show_act[i]).show();
	}

	var formData = new FormData($("#"+frm)[0]);
		  $.ajax({
            url: action_link,
            type: 'POST',
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            success: function (data) {
            
				if(data.MSG=="OK"){
					Swal.fire({position:"top-center",type:"success",title:"Your work has been saved",showConfirmButton:!1,timer:4000})
					setInterval(function(){ window.location.href = data.LINK; }, 2000);
				}else{

					var i;
					for (i = 0; i < hide_act.length; i++) {
					  $('#'+hide_act[i]).show();
					}
					for (i = 0; i < show_act.length; i++) {
					  $('#'+show_act[i]).hide();
					}

					$('#'+error_list).show();
					$('#'+error_list).html(data.MSG);
			
  					$([document.documentElement, document.body]).animate({
				        scrollTop: $("#scrollhere").offset().top
				    }, 1000);
  					
				}
            },
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					var i;
					for (i = 0; i < hide_act.length; i++) {
					  $('#'+hide_act[i]).show();
					}
					for (i = 0; i < show_act.length; i++) {
					  $('#'+show_act[i]).hide();
					}

					$('#'+error_list).show();
					$('#'+error_list).html('<i class="bx bxs-comment-error"></i> <a href="#" class="alert-link" id="error_value"> System Error Please reload the system. <br>If  System error still show please contact your System Administrator</a>');

						$([document.documentElement, document.body]).animate({
				        scrollTop: $("#scrollhere").offset().top
				    }, 1000);

				//alert("System Error Please reload the system ");
			
			},
            data: formData,
            cache: false,
			dataType : 'json',
            contentType: false,
            processData: false
        });

	

	
}
//})