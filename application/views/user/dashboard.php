                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h4 class="card-title mb-4">User List</h4>
                                               
                                            </div>
                                            <div class="col-lg-6 text-right">
                                                <button type="button" class="btn btn-sm btn-success waves-effect waves-light font-size-16" data-toggle="modal" data-target="#addNewUserModal" onclick="	$('#txtpass').val(''); 
	$('#txtpass').attr('type', 'password'); "><i class=" bx bx-user-plus font-size-18 align-middle"></i> Add New User</button>
                                            </div>
                                        </div>
                                        <div class="table-responsive" id="update_table">
                                            <table class="table table-centered table-nowrap mb-0">
                                                <thead class="thead-light">
                                                    <tr>
                                                         <?php  
                                                                $del_selected_emp_details_url =  base_url().'dashboard/delete_selected_user/'; 
                                                        ?>
                                                       <th width="1%">
                                                       <div class="custom-control custom-checkbox custom-checkbox-outline custom-checkbox-primary mb-3">
                                                                <input type="checkbox" class="custom-control-input" id="chck_all_user">
                                                                <label class="custom-control-label" for="chck_all_user"></label>    
                                                       <button style="" onclick="deleteSelectedUsrDetails('<?php echo $del_selected_emp_details_url ?>');" type="button" class="btn btn-sm btn-warning waves-effect waves-light" data-toggle="modal" data-target="#myModal"><i class="bx bxs-trash font-size-18 align-middle"></i></button></th>
                                                        </div>
                                                       <th>User ID</th>
                                                        <th>User Type</th>
                                                        <th>User Name</th>
                                                        <th>Date Added</th>
                                                        <th>Date Updated</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>   
                                                    
                                                <?php foreach($UserList as $row ): 
                                                     $disable_delete = false;
                                                    if($row['id']!=$web_user_id) $disable_delete = true;
                                                    ?>
                                                <tr>
                                                        <td> 
                                                        <?php if($disable_delete){ ?>
                                                            <div class="custom-control custom-checkbox custom-checkbox-outline custom-checkbox-primary mb-3">
                                                                <input type="checkbox" class="custom-control-input" id="chck_user<?php echo $row['id']; ?>" name="chk_ser[]" value="<?php echo $row['id']; ?>">
                                                                <label class="custom-control-label" for="chck_user<?php echo $row['id']; ?>"></label>
                                                            </div>
                                                            <?php } ?>
                                                        </td>
                                                        <td><?php echo $row['id']; ?></td>
                                                        <td><?php echo ($row['user_type']==1? "Super Admin": "Admin"); ?></td>
                                                        <td> <?php echo  $row['user_name']; ?></td>
                                                        <td><?php echo $row['datetime_added']; ?> </td>
                                                        <td><?php echo $row['datetime_modified']; ?> </td>
                                        
                                                        <td> 
                                                            <?php  
                                                            
                                                               
                                                                $get_user_details_url =  base_url().'dashboard/get_user_details/'; 
                                                                $del_user_details_url =  base_url().'dashboard/delete_user/';
                                                            ?>
                                                           
                                                            <button onclick="getUserDetails('<?php echo $get_user_details_url ?>','<?php echo $row['id']; ?>');" type="button" class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#editUserModal"><i class="bx bx-edit font-size-18 align-middle"></i></button>
                                                            <?php if($disable_delete){ ?>
                                                                <button onclick="deleteUserDetails('<?php echo $del_user_details_url ?>','<?php echo $row['id']; ?>');" type="button" class="btn btn-sm btn-danger waves-effect waves-light" data-toggle="modal" data-target="#myModal"><i class="bx bxs-trash font-size-18 align-middle"></i></button>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                               <?php endforeach; ?>
                                                   
                                                   
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- end table-responsive -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


                 
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                s
                            </div>
                            <div class="col-sm-6">
                                <div class="text-sm-right d-none d-sm-block">
                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->
            </div>
        </div>
        <!-- END layout-wrapper -->



        