<!--start add New User -->
<div id="addNewUserModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addNewUserLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="addNewUserLabel">Add New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
           
                <?php $attributes = array('class' => 'custom-validation', 'id' => 'addNewUser','name' => 'addNewEmp','methor'=>'post');
                echo form_open('#',$attributes); ?>
                    <div class="form-group row mb-4">  
                        <label for="txtfname" class="col-sm-3 col-form-label">User name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="txtusername" name="txtusername" required autocomplete="off">
                            <code id="userError" class="highlighter-rouge alert-link"></code> 
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="txtlname" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" id="txtpass" name="txtpass" required autocomplete="off">
                            <code id="userPass" class="highlighter-rouge alert-link"></code> 
                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-xs btn-warning waves-effect btn-label waves-light" onclick="generateKey('<?php echo base_url().'dashboard/randompass' ?>');"><i class="bx bx-key label-icon "></i> Generate</button>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="txtlname" class="col-sm-3 col-form-label">User Type</label>
                        <div class="col-sm-9">
                                <select class="form-control" name="txtusertype" id="txtusertype" required>
                                    <option value="">Select</option>
                                    <option value="1">Super Admin</option>
                                    <option value="2">Admin</option>
                                </select>
                                <code id="userType" class="highlighter-rouge alert-link"></code> 
                        </div>
                    </div>
                   
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <?php
                $submit_url = base_url().'dashboard/save_user';
                ?>
                <button type="button" id="save_btn" class="btn btn-primary waves-effect waves-light"  onclick="actionBtn('addNewUser','<?php echo  $submit_url ?>');">Save</button>
                

                <i style="display:none" id="loader_spinner">
                                                    <div class="spinner-border text-danger m-1" role="status" >
                                                        <span class="sr-only">Loading Please Wait...</span>
                                                    </div>Loading Please Wait...
                                                    </i>

                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>
<!--end add New User -->


<!--start edit User -->
<div id="editUserModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <?php $attributes = array('class' => 'custom-validation', 'id' => 'editUser','name' => 'editEmp','methor'=>'post');
                echo form_open('#',$attributes); ?>
                <input type="hidden" class="form-control" id="txteditid" name="txteditid" >
                  <div class="form-group row mb-4">  
                      
                        <label for="txtfname" class="col-sm-3 col-form-label">User name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="txteditusername" name="txteditusername"  autocomplete="off">
                            <code id="userEditError" class="highlighter-rouge alert-link"></code> 
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="txtlname" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" id="txteditpass" name="txteditpass" autocomplete="off">
                            <code id="userEditPass" class="highlighter-rouge alert-link"></code> 
                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-xs btn-warning waves-effect btn-label waves-light" onclick="generateKey('<?php echo base_url().'dashboard/randompass' ?>');"><i class="bx bx-key label-icon "></i> Generate</button>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="txtlname" class="col-sm-3 col-form-label">User Type</label>
                        <div class="col-sm-9">
                                <select class="form-control" name="txteditusertype" id="txteditusertype" required>
                                    <option value="">Select</option>
                                    <option value="1">Super Admin</option>
                                    <option value="2">Admin</option>
                                </select>
                                <code id="userEditType" class="highlighter-rouge alert-link"></code> 
                        </div>
                    </div>
                   
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <?php
                $submit_url = base_url().'dashboard/edit_user';
                ?>
                <button type="button" id="edit_btn" class="btn btn-primary waves-effect waves-light"  onclick="actionBtn('editUser','<?php echo  $submit_url ?>');">Save</button>
                

                <i style="display:none" id="loader_spinner">
                                                    <div class="spinner-border text-danger m-1" role="status" >
                                                        <span class="sr-only">Loading Please Wait...</span>
                                                    </div>Loading Please Wait...
                                                    </i>

                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>
<!--end edit User -->


<!--start view User -->
<div id="viewUserModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">View User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <img src="" id="empQR"width='30%'/>
            <?php $attributes = array('class' => 'custom-validation', 'id' => 'editUser','name' => 'editEmp','methor'=>'post');
                echo form_open('#',$attributes); ?>
                 <input type="hidden" class="form-control" id="txteditid" name="txteditid" readonly>
                    <div class="form-group row mb-4">
                        <label for="txtfname" class="col-sm-3 col-form-label">First name:</label>
                        <div class="col-sm-9">
                            <label class="col-form-label" id="txtviewfname" name="txtviewfname" ></label>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="txtlname" class="col-sm-3 col-form-label">Last name:</label>
                        <div class="col-sm-9">
                        <label class="col-form-label" id="txtviewlname" name="txtviewlname" ></label>
                        </div>
                    </div>
                   
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
               
                

            
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>
<!--end view User -->



