                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h4 class="card-title mb-4">Time Record List</h4>
                                               
                                            </div>
                                            <div class="col-lg-6 text-right">
                                               
                                            </div>
                                        </div>
                                        <div class="table-responsive" id="update_table">
                                            <table class="table table-centered table-nowrap mb-0">
                                                <thead class="thead-light">
                                                    <tr>
                                                         <?php  
                                                                $del_selected_emp_details_url =  base_url().'dashboard/delete_selected_employee/'; 
                                                        ?>
                                                       <th width="1%"></th>
                                                        <th>Employee ID</th>
                                                        <th>Name</th>
                                                        <th>Date Added</th>
                                                        <th>Time-In</th>
                                                        <th>Time-Out</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>   
                                                    
                                                <?php foreach($time_record_list as $row ): ?>
                                                <tr>
                                                        <td> 
                                                            
                                                        </td>
                                                        <td><?php echo $row['empid']; ?></td>
                                                        <td> <?php echo  ucwords($row['fname']." ".$row['lname']); ?></td>
                                                        <td><?php echo $row['date_added']; ?> </td>
                                                        <td><?php echo $row['time_in']; ?> </td>
                                                        <td> <?php echo $row['time_out']; ?></td>
                                                        <td> 
                                                           
                                                        </td>
                                                    </tr>
                                               <?php endforeach; ?>
                                                   
                                                   
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- end table-responsive -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


                 
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                s
                            </div>
                            <div class="col-sm-6">
                                <div class="text-sm-right d-none d-sm-block">
                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->
            </div>
        </div>
        <!-- END layout-wrapper -->



        