<?php
class UserModel extends CI_Model {

	public function __construct(){
		$this->load->database();
	}
	
	public function saveUser(){
        $user_id = $this->session->userdata('web_user_id');
        $pass=$this->accessuser->DecryptAcct($this->input->post('txtpass'));
		$data=array(
            "user_name"=>$this->input->post('txtusername'),
            "user_password"=> $pass,
            "user_type"=>$this->input->post('txtusertype'),
        );
        $this->db->insert('users', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
	}

    public function editUser(){
		$data=array(
            "user_name"=>$this->input->post('txteditusername'),
            "user_type"=>$this->input->post('txteditusertype')
        );
        $this->db->where('id', $this->input->post('txteditid'));
        $this->db->update('users', $data);

        if($this->input->post('txteditpass')!=null){
            $chk_pass = $this->getUserDetails($this->input->post('txteditpass'));
            $new_pass=$this->accessuser->DecryptAcct($this->input->post('txteditpass'));
            if($chk_pass !=  $new_pass){
                $data=array(
                    "user_password"=>$new_pass,
                );
                $this->db->where('id', $this->input->post('txteditid'));
                $this->db->update('users', $data);
            }
        }
        return ($this->db->affected_rows() != 1) ? false : true;
	}
    
    public function getUserDetails($userId){
      
        $this->db->from("users");
		$this->db->where("id",$userId); 
		$query = $this->db->get();
		$row = $query->row_array(); 
		if ($query->num_rows() > 0)
		{
			return $row;
	    }else{
		    return "";
		}

    }
    
    public function deleteUser($userId){
        $this->db->delete("users", array('id' => $userId)); 
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    

    public function deleteSelectedUser($userId)
    {
        $this->db->where_in('id',$userId);
        $this->db->delete('users');
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function recordList()
    {
        $this->db->from("users");
		$this->db->order_by("id", "desc"); 
		$query = $this->db->get();
		return $query->result_array();
    }


    
}
?>