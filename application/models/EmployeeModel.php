<?php
class EmployeeModel extends CI_Model {

	public function __construct(){
		$this->load->database();
        $this->load->library('session');
	}
	
	public function saveEmployee(){
        $user_id = $this->session->userdata('web_user_id');
		$data=array(
            "first_name"=>$this->input->post('txtfname'),
            "last_name"=>$this->input->post('txtlname'),
            "created_by"=>$user_id,
        );
        $this->db->insert('employees', $data);
        $insert_id = $this->db->insert_id();
        return ($this->db->affected_rows() != 1) ? false : $insert_id;
	}

    public function editEmployee(){
		$data=array(
            "first_name"=>$this->input->post('txteditfname'),
            "last_name"=>$this->input->post('txteditlname'),
        );
        $this->db->where('id', $this->input->post('txteditid'));
        $this->db->update('employees', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
	}
    
    public function getEmployeeDetails($empId){
      
        $this->db->from("employees");
		$this->db->where("id",$empId); 
		$query = $this->db->get();
		$row = $query->row_array(); 
		if ($query->num_rows() > 0)
		{
			return $row;
	    }else{
		    return "";
		}

    }

    public function getEmployeeTimeRec($empId){
      
        $this->db->from("emp_time_records");
		$this->db->where("employee_id",$empId); 
        $this->db->where("date_added",$this->config->item('local_date'));
		$query = $this->db->get();
		$row = $query->row_array(); 
		if ($query->num_rows() > 0)
		{
			return $row;
	    }else{
		    return "";
		}

    }
    
    public function deleteEmployee($empId){
        $this->db->delete("employees", array('id' => $empId)); 
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    

    public function deleteSelectedEmployee($empId)
    {
        $this->db->where_in('id',$empId);
        $this->db->delete('employees');
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function recordList()
    {

        $this->db->select('emp.*,usr.user_name createdby');
        $this->db->from("employees emp");
        $this->db->join('users usr','emp.created_by = usr.id' , "left");
		$this->db->order_by("id", "desc"); 
		$query = $this->db->get();
		return $query->result_array();
    }

    public function checkDTR($empId){
      
        $this->db->from("emp_time_records");
		$this->db->where("employee_id",$empId); 
        $this->db->where("date_added",$this->config->item('local_date'));
		$query = $this->db->get();
		$row = $query->row_array(); 
		if ($query->num_rows() > 0)
		{
			return $row;
	    }else{
		    return "";
		}

    }

    public function insertEmpTime($empId){
		$data=array(
            "employee_id"=>$empId,
            "date_added"=>$this->config->item('local_date'),
            "time_in"=>$this->config->item('local_time'),
        );
        $this->db->insert('emp_time_records', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function updateEmpTime($empId){
		$data=array(
            "time_out"=>$this->config->item('local_time'),
        );
        $this->db->where('employee_id', $empId);
        $this->db->where('date_added', $this->config->item('local_date'));
        $this->db->update('emp_time_records', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    
}
?>