<?php
class AuthenticationModel extends CI_Model {

	public function __construct(){
		$this->load->database();
		$this->load->library('accessuser');
		
	}
	
	public function signin($username,$password){
		$pass=$this->accessuser->DecryptAcct($password);
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where("user_name",$username);
		$this->db->where("user_password",$pass);
		$query = $this->db->get();
		$row = $query->row_array(); 
		if ($query->num_rows() > 0)
		{
			return $row['id'];
		}else{
			return "";
		}
	}

	

	public function getrecord($field,$id){
		$this->db->select($field);
		$this->db->from('users');
		$this->db->where("id",$id);
		$query = $this->db->get();
		$row = $query->row_array(); 
		if ($query->num_rows() > 0)
		{
			return $row[$field];
		}else{
			return "";
		}
	}


}
?>