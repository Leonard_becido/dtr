<?php
class TimeRecordModel extends CI_Model {

	public function __construct(){
		$this->load->database();
	}
	

    public function recordList()
    {
        $this->db->select('trec.*,emp.id empid ,emp.first_name fname,emp.last_name lname');
        $this->db->from("emp_time_records trec");
        $this->db->join('employees emp','emp.id = trec.employee_id' , "left");
		$this->db->order_by("employee_id desc","date_added desc"); 
		$query = $this->db->get();
		return $query->result_array();
    }


    
}
?>