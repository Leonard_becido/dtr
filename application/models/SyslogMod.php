<?php
class SyslogMod extends CI_Model {

	public function __construct(){
		$this->load->database();
		$this->load->library('accessuser');
        $this->load->library('GetTimeZoneDateTime');
		
	}
	
	public function syslog($refnum,$user_id,$msg,$timezone){

      
       
        $phdate= new DateTime("now",new DateTimeZone("Asia/Manila"));
		$ph_date = $phdate->format("M d, Y");
        $ph_time = $phdate->format("H:i:s A");
        $data = array(
            'refnum'=> $refnum,
            'user_id'=>$user_id,
            'message'=>$msg,
            'date'=> $ph_date,
            'time'=> $ph_time ,
            );
    
    $this->db->insert("tbl_logs",$data);
    
    }
}
?>