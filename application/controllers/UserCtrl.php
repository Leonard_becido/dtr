<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserCtrl extends CI_Controller {
	public $basetemplate;
	public $user_id;
	public $data=array();
	public function __construct()
	{
		 parent::__construct();
		 

		 $this->data['web_user_id'] = $this->session->userdata('web_user_id');
		if($this->data['web_user_id']==""){
			redirect(base_url().'login/authenticate');	
		}
		$this->data['web_user_type'] = $this->session->userdata('web_user_type');
		 
		$this->data["body_title"]="Dashboard";
		$this->data["page_title"]="QR Time Record";
		$this->data["main_css_js_dir"]=base_url().'application/views'.$this->config->item('main_dist');
		$this->data['clock'] =date("D F d, Y h:i:s A",strtotime($this->config->item('local_datetime')));

		$this->include=$this->config->item('include');
		// $this->data["pages_list"]=$this->PageModel->get_page_list();

		$this->user_id = $this->session->userdata('web_user_id');
		// $user_detail = $this->ClientMod->get_profile_details( $this->user_id);
		// $this->data["user_detail"]=$user_detail;

		$this->load->model('UserModel');

		$this->basetemplate = $this->config->item('user');
	}
	public function index()
	{
		
		$this->data['css_include']=array(
			"libs/sweetalert2/sweetalert2.min.css",
		);

		// $this->data['bread_crumb']=array(
		// 	array("breadcrumb-item",'<a href="javascript: void(0);">Dashboard</a>'),
		// 	array("breadcrumb-item active","Dashboard",), 
		// );

		 $this->data['UserList'] = $this->UserModel->recordList();

		$this->data['jsinclude']=array(
			"js/custom/custom.js",
			"js/pages/form-editor.init.js",
			
			
		);
		
		 $this->load->view($this->include.'header',$this->data);
		 $this->load->view($this->basetemplate.'dashboard',$this->data);
		 $this->load->view($this->basetemplate.'modal',$this->data);
		 $this->load->view($this->include.'footer',$this->data);
	}

	public function password_check($str)
	{
		if (preg_match('#[0-9]#', $str) && preg_match('#[A-Z]#', $str) && preg_match('#[a-z]#', $str) && preg_match("/[\'^£$%&*()}{@#~?><>,|=_+¬-]/", $str) ){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function saveNewUser()
	{
		
		$this->form_validation->set_rules('txtusername', 'User Name', 'required|is_unique[users.user_name]',
				array(
					'required'      => 'You have not provided %s.',
					'is_unique'     => 'This %s already exists.'
			)
		);

		$this->form_validation->set_rules('txtpass', 'Password', 'required|min_length[10]|callback_password_check',
				array(
					'required'      => 'You have not provided %s.',
					'min_length'     => 'This %s already exists.',
					'password_check'=> 'Password must be 15 character,atleast 1 special Character and alpanumeric'
				)
		);

		$this->form_validation->set_rules('txtusertype', 'User Type', 'required',
				array(
					'required'      => 'You have not provided %s.',
			)
		);
		$this->form_validation->run();
		
		if (validation_errors() != ""){
			$status = "error";
		}else{
			 $userId = $this->UserModel->saveUser();
			if($userId){
				$status = "success";
			}else{
				$status = "error";
			}
		}

		echo json_encode(array(
			"STATUS"=>	$status,
			"ERRORUSER"=> form_error('txtusername'),
			"ERRORPASS"=> form_error('txtpass'),
			"ERRORTYPE"=> form_error('txtusertype')
		));

	}

	

	public function editUser()
	{
		$userDetails = $this->UserModel->getUserDetails($this->input->post('txteditid'));
		if($userDetails['user_name']!=$this->input->post('txteditusername')){
			$this->form_validation->set_rules('txteditusername', 'User Name', 'required|is_unique[users.user_name]',
					array(
						'required'      => 'You have not provided %s.',
						'is_unique'     => 'This %s already exists.'
				)
			);
		}

		if($this->input->post('txteditpass')!=null){
			$this->form_validation->set_rules('txteditpass', 'Password', 'min_length[10]|callback_password_check',
					array(
						'min_length'     => 'This %s already exists.',
						'password_check'=> 'Password must be 15 character,atleast 1 special Character and alpanumeric'
					)
			);
		}

		$this->form_validation->set_rules('txteditusertype', 'User Type', 'required',
				array(
					'required'      => 'You have not provided %s.',
			)
		);
		$this->form_validation->run();
		
		if (validation_errors() != ""){
			$status = "error";
		}else{
			if($this->UserModel->editUser()){
				$status = "success";
			}else{
				$status = "error";
			}
		}

		echo json_encode(array(
			"STATUS"=>$status,
			"ERRORUSER"=> form_error('txteditusername'),
			"ERRORPASS"=> form_error('txteditpass'),
			"ERRORTYPE"=> form_error('txteditusertype')
		));

	}

	public function UserDetails($userId)
	{
		if($userId){
			$details = $this->UserModel->getUserDetails($userId);
			echo json_encode(array(
				"STATUS"=>"success",
				"USERID"=>$details['id'],
				"USERNAME"=>$details['user_name'],
				"USERTYPE"=>$details['user_type']
			));
		}else{
			echo json_encode(array(
				"STATUS"=>"error",
				"USERID"=>'',
				"USERNAME"=>'',
				"USERTYPE"=>''
			));
		}
	}

	public function deleteUser($empId)
	{
		if($this->UserModel->deleteUser($empId)){
			$status = "success";
		}else{
			$status = "error";
		}

		echo json_encode(array(
			"STATUS"=>$status
		));
	}

	public function deleteSelectedUser()
	{
		
		$empId = $this->input->post("selected_chk");
	
		$status = "error";
		if($empId){
			if($this->UserModel->deleteSelectedUser($empId)){
				$status = "success";
			}else{
				$status = "error";
			}
		}
	
		echo json_encode(array(
			"STATUS"=>$status
		));
		
	}

	function random_password() 
	{
		$alphabet = str_split('abcdefghijklmnopqrstuvwxyz');
		$Alphabet = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$numeric = str_split('1234567890');
		$special = str_split('\'^$%&*()}{@#~?><>,|=_+¬-');
		
		$id_length = 16;

		$rand_key = "";
		for($i = 1; $i < $id_length; $i ++) {
			if($i >=1 && $i <= 4){
				$keys = 'abcdefghijklmnopqrstuvwxyz';
			}elseif($i >=5 && $i <= 8){
				$keys = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			}elseif($i >=9 && $i <= 12){
				$keys = '1234567890';
			}else{
				$keys = '\'^$%&*()}{@#~?><>,|=_+-';
			}

		@$rand_key .= $keys[rand(1, strlen($keys))];

		}
		$keys2 = $rand_key;
		$rand_key="";
		for($i = 1; $i < $id_length; $i++) {
		
			@$rand_key .= $keys2[rand(1, strlen($keys2))];
		}
	

		echo json_encode(array(
			"PASSKEY"=>$rand_key
		));
		

	}

	
	
}
