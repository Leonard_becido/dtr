<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TimeRecordCtrl extends CI_Controller {
	public $basetemplate;
	public $user_id;
	public $data=array();
	public function __construct()
	{
		 parent::__construct();
		 

		 $this->data['web_user_id'] = $this->session->userdata('web_user_id');
		if($this->data['web_user_id']==""){
			redirect(base_url().'login/authenticate');	
		}
        $this->data['web_user_type'] = $this->session->userdata('web_user_type');
		 
		$this->data["body_title"]="Dashboard";
		$this->data["page_title"]="QR Time Record";
		$this->data["main_css_js_dir"]=base_url().'application/views'.$this->config->item('main_dist');
		$this->data['clock'] =date("D F d, Y h:i:s A",strtotime($this->config->item('local_datetime')));

		$this->include=$this->config->item('include');
		// $this->data["pages_list"]=$this->PageModel->get_page_list();

		$this->user_id = $this->session->userdata('web_user_id');
		// $user_detail = $this->ClientMod->get_profile_details( $this->user_id);
		// $this->data["user_detail"]=$user_detail;

		$this->load->model('TimeRecordModel');

		$this->basetemplate = $this->config->item('time_record');
	}
	public function index()
	{
		
		$this->data['css_include']=array(
			"libs/sweetalert2/sweetalert2.min.css",
		);

		// $this->data['bread_crumb']=array(
		// 	array("breadcrumb-item",'<a href="javascript: void(0);">Dashboard</a>'),
		// 	array("breadcrumb-item active","Dashboard",), 
		// );

		 $this->data['time_record_list'] = $this->TimeRecordModel->recordList();

		$this->data['jsinclude']=array(
			"js/custom/custom.js",
			"js/pages/form-editor.init.js",
			
			
		);
		
		 $this->load->view($this->include.'header',$this->data);
		 $this->load->view($this->basetemplate.'dashboard',$this->data);
		 $this->load->view($this->basetemplate.'modal',$this->data);
		 $this->load->view($this->include.'footer',$this->data);
	}

	
}
