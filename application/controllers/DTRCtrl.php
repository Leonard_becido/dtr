<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DTRCtrl extends CI_Controller {
	public $include;
	public $user_id;
	public $data=array();
	public function __construct()
	{
		 parent::__construct();

         $this->data["body_title"]="Dashboard";
		 $this->data["page_title"]="QR Time Record";
		 $this->data["main_css_js_dir"]=base_url().'application/views'.$this->config->item('main_dist');
         $this->data['clock'] =date("D F d, Y h:i:s A",strtotime($this->config->item('local_datetime')));

         $this->include=$this->config->item('include');

         $this->load->model("AuthenticationModel");
		 $this->load->model("EmployeeModel");

	}
	public function index()
	{
		if($this->session->userdata("web_user_id")){
			redirect(base_url()."dashboard");
		}
		 $this->load->view('qr_signin',$this->data);
	}


    public function signin($action=null){
		if($this->session->userdata("web_user_id")){
			redirect(base_url()."dashboard");
		}

		$username = $this->input->post('txtusername');
		$passwrd =$this->input->post('txtuserpassword');

			
		$this->data['cssinclude']=array(

		);
		$this->data['logerror']="";
		if($action == "authentication"){
			// echo $passssss=$this->accessuser->DecryptAcct($this->input->post('txtuserpassword'));
			$userlog = $this->AuthenticationModel->signin($username,$passwrd);
			if($userlog!=""){
				$user_type=$this->AuthenticationModel->getrecord("user_type",$userlog);
				$this->session->set_userdata('web_user_id',$userlog);
				$this->session->set_userdata('web_user_type',$user_type);

				if($user_type==1){
					redirect(base_url().'dashboard');
				}else{
					redirect(base_url().'dashboard/time_record');
				}
			}else{
				$this->data['logerror']="Invalid Username or Password.";

			}
		}

		


		$this->load->view('qr_signin',$this->data);
	}


	public function timInOut()
	{
		$empId = $this->input->post('QRCode');
		$check_QR = $this->EmployeeModel->getEmployeeDetails($empId);
		if($check_QR != ""){
			$check_current_date = $this->EmployeeModel->checkDTR($empId);
			if($check_current_date==""){
				$this->EmployeeModel->insertEmpTime($empId);
				$get_time = $this->EmployeeModel->getEmployeeTimeRec($empId);
				$emptime= "TIME-IN : ".$get_time['date_added']." ".$get_time['time_in'];
			}else{
				$this->EmployeeModel->updateEmpTime($empId);
				$get_time = $this->EmployeeModel->getEmployeeTimeRec($empId);
				$emptime= "TIME-OUT : ".$get_time['date_added']." ". $get_time['time_out'];
			}
			$status = "success";
			$empName = $check_QR['first_name']." ".$check_QR['last_name'];
			
		}else{
			$status = "not found";
			$empName = "";
			$emptime= "";
		}
		echo json_encode(array(
			"STATUS"=>$status,
			"EMPNAME"=>$empName,
			"EMPTIME"=>$emptime
		));
	}

	public function signout(){
		$this->session->unset_userdata('web_user_id');
		redirect(base_url());

	}

	public function error_page()
	{
		$this->load->view($this->basetemplate_admin.'include/header',$this->data);
		$this->load->view($this->basetemplate_admin.'dashboard',$this->data);
		$this->load->view($this->basetemplate_admin.'include/footer',$this->data);
	}


	function isLogin(){
	 
		if($this->session->userdata("web_user_id")){
			redirect(base_url());
		}
	}
}
