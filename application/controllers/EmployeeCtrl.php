<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeCtrl extends CI_Controller {
	public $basetemplate;
	public $user_id;
	public $data=array();
	public function __construct()
	{
		 parent::__construct();
		 

	    $this->data['web_user_id'] = $this->session->userdata('web_user_id');
		if($this->data['web_user_id']==""){
			redirect(base_url().'login/authenticate');	
		}
		$this->data['web_user_type'] = $this->session->userdata('web_user_type');
		if($this->data['web_user_type']!=1){
			
			redirect(base_url().'dashboard/time_record');
		}

		 
		$this->data["body_title"]="Dashboard";
		$this->data["page_title"]="QR Time Record";
		$this->data["main_css_js_dir"]=base_url().'application/views'.$this->config->item('main_dist');
		$this->data['clock'] =date("D F d, Y h:i:s A",strtotime($this->config->item('local_datetime')));

		$this->include=$this->config->item('include');
		// $this->data["pages_list"]=$this->PageModel->get_page_list();

		$this->user_id = $this->session->userdata('web_user_id');
		// $user_detail = $this->ClientMod->get_profile_details( $this->user_id);
		// $this->data["user_detail"]=$user_detail;

		$this->load->model('EmployeeModel');

		$this->basetemplate = $this->config->item('employee');
	}
	public function index()
	{
		
		$this->data['css_include']=array(
			"libs/sweetalert2/sweetalert2.min.css",
		);

		// $this->data['bread_crumb']=array(
		// 	array("breadcrumb-item",'<a href="javascript: void(0);">Dashboard</a>'),
		// 	array("breadcrumb-item active","Dashboard",), 
		// );

		 $this->data['employeeList'] = $this->EmployeeModel->recordList();

		$this->data['jsinclude']=array(
			"js/custom/custom.js",
			"js/pages/form-editor.init.js",
			
			
		);
		
		 $this->load->view($this->include.'header',$this->data);
		 $this->load->view($this->basetemplate.'dashboard',$this->data);
		 $this->load->view($this->basetemplate.'modal',$this->data);
		 $this->load->view($this->include.'footer',$this->data);
	}

	public function saveNewEmployee()
	{
		$empId = $this->EmployeeModel->saveEmployee();
		if($empId){
			$this->qrcodeGenerator($empId);
			$status = "success";
		}else{
			$status = "error";
		}

		echo json_encode(array(
			"STATUS"=>$status,
			"EMPID"=>$empId
		));

	}

	public function qrcodeGenerator($empId)
	{
		$SERVERFILEPATH = FCPATH.'/qrcode/';
		$params['data'] = $empId;
		$params['level'] = 'H';
		$params['size'] = 5;
		$params['savename'] =$SERVERFILEPATH.$empId.'.png';
		$this->ciqrcode->generate($params);

		
	}

	public function editEmployee()
	{
		if($this->EmployeeModel->editEmployee()){
			$status = "success";
		}else{
			$status = "error";
		}

		echo json_encode(array(
			"STATUS"=>$status
		));
	}

	public function employeeDetails($empId)
	{
		if($empId){
			$details = $this->EmployeeModel->getEmployeeDetails($empId);
			echo json_encode(array(
				"STATUS"=>"success",
				"EMPID"=>$details['id'],
				"EMPFNAME"=>$details['first_name'],
				"EMPLNAME"=>$details['last_name']
			));
		}else{
			echo json_encode(array(
				"STATUS"=>"error",
				"EMPID"=>'',
				"EMPFNAME"=>'',
				"EMPLNAME"=>''
			));
		}
	}

	public function deleteEmployee($empId)
	{
		if($this->EmployeeModel->deleteEmployee($empId)){
			$status = "success";
			unlink("qrcode/".$empId.".png");
		}else{
			$status = "error";
		}

		echo json_encode(array(
			"STATUS"=>$status
		));
	}

	public function deleteSelectedEmployee()
	{
		
		$empId = $this->input->post("selected_chk");
	
		$status = "error";
		if($empId){
			if($this->EmployeeModel->deleteSelectedEmployee($empId)){
				$status = "success";
			}else{
				$status = "error";
			}
		}
	
		echo json_encode(array(
			"STATUS"=>$status
		));
		
	}
	
	
	public function error_page()
	{
		$this->load->view($this->basetemplate_admin.'include/header',$this->data);
		$this->load->view($this->basetemplate_admin.'dashboard',$this->data);
		$this->load->view($this->basetemplate_admin.'include/footer',$this->data);
	}
}
